## Projet BigData et Visualisation
## Description

Ce projet traite des données venant de l'api "Données hospitalières relatives à l'épidémie de COVID-19 en Île-de-France"
Lien du site de l'api : Données hospitalières relatives à l'épidémie de COVID-19 en Île-de-France
Cette api est mise à jour tout les jour et nous souhaitons avec projet mettre en place un systeme récupérant (kafka), traitant (spark) et affichant (pandas) les informations de cette api.


Notre applicapion se compose d'un dossier Docker comportant les paramètres docker notament kafka, spark et python. 

D'un dossier Scripts comportant deux producer envoyant les données en fonction de la colonne sexe (le premier consummer renvois les données global alors que le second différencie les données concernant les femmes ou les hommes). 
Un scripte consummer qui récupère avec spark.
Un scripte traitement qui traites les données avec spark.
Un scripte PandasExemple qui a servie de note pour préparer les graphiques Pandas, ainsi qu'un nootbook PandasPresentation qui a servi de test de visualisation Pandas.
Et un scripte app qui fait génère les routes Flask qui nous permettent d'avvicher une page HTML avec nos visuel confertit en png.

Et d'un dossier Template qui comporte notre template HTML vers lequel pointe le scripte app, dans lequel nous affichons nos grafiques sous forme d'image PNG (convertit avec spark dans le app)

#Installation dans les images docker :

flask : 

- pip install flask

pour lancer le script :

- python /home/front/app.py

python : 

- pip install pyspark kafka-python pandas matplotlib
- apt update
- apt-install default-jdk
- export PYSPARK_PYTHON=/usr/local/bin/python
- export PYSPARK_DRIVER_PYTHON=/usr/local/bin/python
- source ~/.bashrc

pour lancer le script :

- python /home/script/consummerHF.py
=======

## Description

Ce projet traite des données venant de l'api "Données hospitalières relatives à l'épidémie de COVID-19 en Île-de-France"
Lien du site de l'api : Données hospitalières relatives à l'épidémie de COVID-19 en Île-de-France
Cette api est mise à jour tout les jour et nous souhaitons avec projet mettre en place un systeme récupérant (kafka), traitant (spark) et affichant (pandas) les informations de cette api.


Notre applicapion se compose d'un dossier Docker comportant les paramètres docker notament kafka, spark et python. 

D'un dossier Scripts comportant deux producer envoyant les données en fonction de la colonne sexe (le premier consummer renvois les données global alors que le second différencie les données concernant les femmes ou les hommes). 
Un scripte consummer qui récupère avec spark.
Un scripte traitement qui traites les données avec spark.
Un scripte PandasExemple qui a servie de note pour préparer les graphiques Pandas, ainsi qu'un nootbook PandasPresentation qui a servi de test de visualisation Pandas.
Et un scripte app qui fait génère les routes Flask qui nous permettent d'avvicher une page HTML avec nos visuel confertit en png.

Et d'un dossier Template qui comporte notre template HTML vers lequel pointe le scripte app, dans lequel nous affichons nos grafiques sous forme d'image PNG (convertit avec spark dans le app)

## Membre du projet
axel.storme@ynov.com

rafael.metayer@ynov.com

basma.eziani@ynov.com

seynabou.fall@ynov.com
