#Imports
from kafka import KafkaConsumer
from json import loads
from time import sleep
import pandas as pd
from pyspark.sql import SparkSession
from pyspark import SparkContext, SparkConf
import matplotlib.pyplot as plt

def barChart(data):
    pandaRepMortDept = data.toPandas()
    plt.bar(pandaRepMortDept['departement'], pandaRepMortDept['sum(total_mort)'], color='m')
    plt.xticks(rotation=45)
    plt.subplots_adjust(bottom=0.4)
    plt.savefig('./img/totalMortDept.png', dpi=200)
    plt.clf()

def pieChart(data):
    pandaRepVeilleHFIntens = data.toPandas()
    plt.pie(pandaRepVeilleHFIntens['sum(intensif_now)'], labels=pandaRepVeilleHFIntens['sexe'])
    plt.savefig('./img/veilleIntensifHF.png', dpi=200)
    plt.clf()

def plotChart(data):
    pandaRepDateHopital = data.toPandas()
    plt.plot(pandaRepDateHopital['date'], pandaRepDateHopital['sum(hopital_now)'], color='r')
    plt.xticks(rotation=90, ha='right')
    plt.subplots_adjust(bottom=0.4)
    plt.savefig('./img/recapHopital.png', dpi=200)
    plt.clf()

#Fonction qui permet de traiter le dataframe python, faire des calculs et les transformr en grpahs panda.
def create_graphs(dataCovid, path):

    # Récupération dernière date
    maxDate = dataCovid.agg({"date": "max"}).collect()
    date_vals = [r['max(date)'] for r in maxDate]
    date_vals = str(date_vals)
    date_vals = date_vals.replace("[", "").replace("]", "").replace("'", "")

    # Repartition Homme / Femme des hospitalisations des personnes en soins intensif de la derniere date
    repartitionMortDept = dataCovid.filter(dataCovid["date"] == str(date_vals)).groupby("departement").agg(
        {"total_mort": "sum"})
    barChart(repartitionMortDept)

    #Repartition quotidienne des personnes hospitalisées
    repartitionDateHopital = dataCovid.groupby("date").agg({"hopital_now" : "sum"}).sort("date")
    plotChart(repartitionDateHopital)



    # Repartition Homme / Femme des hospitalisations des personnes en soins intensif de la derniere date
    repartitionVeilleIntensifHF = dataCovid.filter(dataCovid["date"] == str(date_vals)).groupby("sexe").agg({"intensif_now" : "sum"})
    pieChart(repartitionVeilleIntensifHF)


    pandasDF = dataCovid.toPandas()



#Création de la session Spark
spark = SparkSession.builder.getOrCreate()

#Topic Kafka
consumer = KafkaConsumer(
    'covidHF',
    bootstrap_servers=['kafka:9093'],
    value_deserializer=lambda x: loads(x.decode('utf-8'))
)

#Gestion de la session spark
conf = SparkConf().setAppName("covidData").setMaster("local[*]")
sc = SparkContext.getOrCreate(conf)

#Chemin des fichiers et récupération de ceux-ci
path = "./data/covid/covidHF_IleDeFrance"
dataCovid = spark.read.load(path, format="json")
dataCovid.dtypes

create_graphs(dataCovid, path)

#Pour chaque evenement dans le topic
for event in consumer:
    #Stockage de l'event
    data = event.value

    #Transformation de la donnée reçue en datframe spark
    df2 = spark.read.json(sc.parallelize([data]))
    df = df2.toDF("date", "departement", "hopital_now", "id", "intensif_now", "region", "sexe", "total_mort",
                  "total_sortie")

    #Récupération de l'id reçu
    idDF = df.agg({"id" : "max"}).collect()
    id_vals = [r['max(id)'] for r in idDF]
    id_vals = str(id_vals)
    id_vals = id_vals.replace("[","").replace("[", "").replace("]", "").replace("'", "")

    #Recherche de l'Id Reçue dans toutes les données sauvegardées
    countId = dataCovid.filter(dataCovid["id"] == str(id_vals)).count()

    #Si l'id n'existe pas, sauvegarde du df et ajout de celui-ci au DataFrame global
    if countId == 0:
        print("saved")
        df.write.mode("append").save("/home/data/covid/covidHF_IleDeFrance", format="json")
        dataCovid.unionAll(df)

    #Methode création des graphs
    create_graphs(dataCovid, path)

    sleep(0.5)


