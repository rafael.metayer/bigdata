#imports
from time import sleep
import json, requests
from kafka import KafkaProducer
from json import dumps

#dernière date des données
lastrecord = None
rowsNumber = "24"
sort = "date"

#Création du producer
producer = KafkaProducer(
    bootstrap_servers=['localhost:9092'],
    value_serializer=lambda x: dumps(x).encode('utf-8')
)

#Tant que le programme tourne
while True:

    #Url de l'api
    url = "https://data.iledefrance.fr/api/records/1.0/search/?dataset=donnees-hospitalieres-relatives-a-lepidemie-de-covid-19-en-france&q=&rows=140&sort=date&facet=date&facet=countrycode_iso_3166_1_alpha3&facet=region_min&facet=nom_dep_min&facet=sex"
    #Récupérer les données
    request = requests.get(url)
    #Les convertir en texte
    text = request.text
    #Puis sous format JSON
    data = json.loads(text)
    #Stocker la partie résultats
    records = data['records']

    #Si le dernier élement n'existe pas ou est différent de l'élément récupéré
    if lastrecord != records[0]['recordid']:
        #Redéfinir le dernier élément
        lastrecord = records[0]['recordid']
        #Traiter les éléments
        for element in data['records']:
            #Si le sexe est Homme ou Femme
            if element['fields']['sex'] == 'Homme' or element['fields']['sex'] == 'Femme':
                #Préparer les données à envoyer au consumer
                data_send = {'id': element['recordid'],
                             'date': element['fields']['date'],
                             'region': element['fields']['region_min'],
                             'departement': element['fields']['nom_dep_min'],
                             'sexe': element['fields']['sex'],
                             'hopital_now': element['fields']['day_hosp'],
                             'intensif_now': element['fields']['day_intcare'],
                             'total_sortie': element['fields']['tot_out'],
                             'total_mort': element['fields']['tot_death']
                             }
                #Afficher ce qui est à envoyer
                print(data_send)
                #Envoyer les données sur le topic covidHF
                producer.send('covidHF', value=data_send)
                #Patienter
                sleep(0.5)
    else:
        #Patienter
        print("waiting")
    sleep(5)