from kafka import KafkaConsumer
from json import loads
from time import sleep

import os.path

from pyspark.sql import SparkSession

spark = SparkSession\
    .builder\
    .master("local[*]")\
    .appName("covidData")\
    .config("spark.jars.packages", "org.apache.spark:spark-sql-kafka-0-10_2.12:3.1.1")\
    .getOrCreate()

sc = spark.sparkContext

lines = spark.readStream.format("kafka").option("kafka.bootstrap.servers","kafka:9093").option("subscribe","covidHF").load()

lines = lines.selectExpr("CAST(value AS STRING) as json")

query = lines.writeStream.format("json").trigger(processingTime="1000 seconds").option("checkpointLocation", "/data/covid/covidHF_IleDeFrance").option("path", "/data/covid/covidHF_IleDeFrance/data").outputMode("append").start()
lines.printSchema()
lines.take(1)
#spark.readStream.schema("json").json("/data/covid/covidHF_IleDeFrance/data")

query.awaitTermination()
